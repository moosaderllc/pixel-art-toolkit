import pygame, sys
from pygame.locals import *

#frameCount = input( "How many frames? " )
frameCount = 4

pygame.init()
fpsClock = pygame.time.Clock()

screenWidth = 640
screenHeight = 480

window = pygame.display.set_mode( ( 640, 480 ) )
pygame.display.set_caption( "Moosader Sprite Animator" )

bgColor = pygame.Color( 200, 200, 200 )

frames = []
doubleFrames = []

for f in range( frameCount ):
    newFrame = pygame.image.load( "Input/" + str( f+1 ) + ".png" )
    frames.append( newFrame )

    width, height = newFrame.get_rect().size
    newBigFrame = pygame.transform.scale( newFrame, ( width*2, height*2 ) )
    doubleFrames.append( newBigFrame )

width, height = frames[0].get_rect().size
quarterX = screenWidth/4 - width/2

speeds = [ 0.01, 0.05, 0.1, 0.5, 1 ]

anim = 0
animSpeed = 1

print( "Animation Speed: " + str( speeds[ animSpeed ] ) )

while ( True ):
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()

        elif ( event.type == KEYDOWN ):
            if ( event.key == K_EQUALS ):
                animSpeed += 1
                if ( animSpeed >= len( speeds ) ):
                    animSpeed = 0

                print( "Animation Speed: " + str( speeds[ animSpeed ] ) )
                    
            elif ( event.key == K_MINUS ):
                animSpeed -= 1
                if ( animSpeed < 0 ):
                    animSpeed = len( speeds ) - 1

                print( "Animation Speed: " + str( speeds[ animSpeed ] ) )
                    
    
    window.fill( bgColor )
    window.blit( frames[int(anim)], ( quarterX, screenHeight/2 - height/2 ) )
    window.blit( doubleFrames[int(anim)], ( quarterX * 3, screenHeight/2 - height ) )
    pygame.display.update()

    fpsClock.tick( 60 )
    
    anim += speeds[ animSpeed ]
    if ( anim >= frameCount ):
        anim = 0
